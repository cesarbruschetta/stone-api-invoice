FROM python:3.7-alpine AS build
COPY . /src
RUN pip install --upgrade pip \
    && pip install wheel
RUN cd /src \
    && python setup.py bdist_wheel -d /deps

FROM python:3.7-alpine

COPY --from=build /deps/* /deps/
COPY production.ini /app/config.ini
COPY requirements.txt .

RUN apk add --update --no-cache mariadb-connector-c-dev \
	&& apk add --no-cache --virtual .build-deps \
		mariadb-dev gcc musl-dev \
    && pip --no-cache-dir install -r requirements.txt \
    && pip install --no-index --find-links=file:///deps -U challenge-invoiceapi \
    && rm -rf .cache/pip \
    && apk --purge del .build-deps \
    && rm requirements.txt \
    && rm -rf /deps

EXPOSE 8080
WORKDIR /app
USER nobody
CMD ["pserve", "/app/config.ini"]