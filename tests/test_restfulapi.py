""" """
import json
import unittest
from unittest.mock import patch, Mock, ANY, PropertyMock
from datetime import datetime
from decimal import Decimal

import MySQLdb
from webtest import TestApp

from invoiceapi import restfulapi
from invoiceapi.exceptions import DoesNotExist
from tests.fixtures import INVOICES


class TestRestfulApi(unittest.TestCase):
    def setUp(self):

        MySQLdb_patcher = patch.object(restfulapi, "MySQLdb", Mock(wraps=MySQLdb))
        mocked_MySQLdb = MySQLdb_patcher.start()

        self.mock_db = Mock()
        mocked_MySQLdb.connect.return_value = self.mock_db

        self.settings = {
            "api.mysqldb.user": "test",
            "api.mysqldb.password": "123456",
            "api.mysqldb.host": "host_db",
            "api.mysqldb.database": "test_invoice",
            "api.mysqldb.port": 3306,
            "api.auth.token": "12345678",
        }
        self.headers = {
            "X-API-Token": "12345678",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
        self.app = TestApp(restfulapi.main({}, **self.settings))
        self.addCleanup(MySQLdb_patcher.stop)

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_get_status_ok_case_page_1_not_filter_not_order(
        self, Mk_Invoice
    ):

        mk_obj_invoice = Mock()
        mk_obj_invoice.count.return_value = 100
        mk_obj_invoice.filter.return_value = INVOICES
        Mk_Invoice.return_value = mk_obj_invoice

        self.app.get("/invoices", status=200, headers=self.headers)
        mk_obj_invoice.filter.assert_called_once_with(
            query={}, order=[], size=30, offset=0
        )

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_get_status_ok_case_page_n_with_filter_with_order(
        self, Mk_Invoice
    ):

        mk_obj_invoice = Mock()
        mk_obj_invoice.count.return_value = 100
        mk_obj_invoice.filter.return_value = INVOICES
        Mk_Invoice.return_value = mk_obj_invoice
        query = {"ReferenceMonth": "10", "order_by": "ReferenceYear", "page": "2"}

        self.app.get("/invoices", status=200, headers=self.headers, params=query)
        mk_obj_invoice.filter.assert_called_once_with(
            query={"ReferenceMonth": "10"}, order=["ReferenceYear"], size=30, offset=30
        )

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_post_status_ok(self, Mk_Invoice):

        mk_obj_invoice = Mock()
        type(mk_obj_invoice).Id = PropertyMock(return_value=1)
        Mk_Invoice.return_value = mk_obj_invoice

        data = {
            "ReferenceMonth": 10,
            "ReferenceYear": 2019,
            "Document": "DOC001",
            "Description": "DOC Invoce Test 1",
            "Amount": 100.00,
        }
        response = self.app.post(
            "/invoices", status=201, params=json.dumps(data), headers=self.headers
        )
        self.assertEqual(response.json["id"], 1)

        result = data.copy()
        result["Amount"] = Decimal(100.0)
        Mk_Invoice.assert_called_once_with(result, ANY)
        mk_obj_invoice.create.assert_called_once_with()

    def test_restfulapi_invoices_post_status_bad_request_data_invalid(self):

        data = {
            "ReferenceMonth": 10,
            "ReferenceYear": 2019,
            "Document": "DOC001",
            "Description": "DOC Invoce Test 1",
        }
        self.app.post(
            "/invoices", status=400, params=json.dumps(data), headers=self.headers
        )

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_post_status_bad_request_with_except(self, Mk_Invoice):

        mock = Mock()
        mock.Id = 1
        mock.create.side_effect = Exception("Test Error")
        Mk_Invoice.return_value = mock

        data = {
            "ReferenceMonth": 10,
            "ReferenceYear": 2019,
            "Document": "DOC001",
            "Description": "DOC Invoce Test 1",
            "Amount": 100.00,
        }
        self.app.post(
            "/invoices", status=400, params=json.dumps(data), headers=self.headers
        )

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_delete_status_ok(self, Mk_Invoice):

        mk_obj_invoice = Mock()
        Mk_Invoice.return_value = mk_obj_invoice

        data = {"DeactiveAt": "2019-09-20T15:00:00"}
        self.app.delete(
            "/invoices/1", status=204, params=json.dumps(data), headers=self.headers
        )
        mk_obj_invoice.delete.assert_called_once_with()

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_delete_status_not_found(self, Mk_Invoice):

        mock = Mock()
        mock.delete.side_effect = DoesNotExist("Not Invoice")
        Mk_Invoice.return_value = mock

        data = {"DeactiveAt": "2019-09-20T15:00:00"}
        self.app.delete(
            "/invoices/1", status=404, params=json.dumps(data), headers=self.headers
        )

    @patch("invoiceapi.restfulapi.Invoce")
    def test_restfulapi_invoices_delete_status_bad_request_with_except(
        self, Mk_Invoice
    ):

        mock = Mock()
        mock.delete.side_effect = Exception("Test delete Error")
        Mk_Invoice.return_value = mock

        data = {"DeactiveAt": "2019-09-20T15:00:00"}
        self.app.delete(
            "/invoices/1", status=400, params=json.dumps(data), headers=self.headers
        )
