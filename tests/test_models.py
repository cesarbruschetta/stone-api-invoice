""" """
import unittest
from unittest.mock import Mock, patch, ANY, MagicMock
from decimal import Decimal
from datetime import datetime

from invoiceapi.models import Invoce
from invoiceapi.exceptions import AlreadyExists, DoesNotExist, NonDatabaseConnection


class TestInvoce(unittest.TestCase):
    def setUp(self):

        self.data = {
            "CreatedAt": datetime.now(),
            "ReferenceMonth": "10",
            "ReferenceYear": "2019",
            "Document": "DOC001",
            "Description": "DOC Invoce Test 1",
            "Amount": 100,
            "IsActive": True,
            "DeactiveAt": None,
        }

    def test_Invoce_init(self):
        obj = Invoce(self.data, Mock())

        self.assertEqual(obj.ReferenceMonth, 10)
        self.assertEqual(obj.ReferenceYear, 2019)
        self.assertEqual(obj.Document, "DOC001")
        self.assertEqual(obj.Description, "DOC Invoce Test 1")

        self.assertEqual(obj.Amount, Decimal(100))
        self.assertIsInstance(obj.Amount, Decimal)

    def test_Invoce_call_connector(self):

        mk_conn = Mock()
        obj = Invoce(self.data, mk_conn)
        conn = obj.connector
        self.assertEqual(conn, mk_conn)

    def test_Invoce_call_connector_with_error_NonDatabaseConnection(self):

        obj = Invoce(self.data, None)
        self.assertRaises(NonDatabaseConnection, getattr, obj, "connector")

    def test_Invoce_call_create(self):

        mk_conn = Mock()
        mk_cursor = Mock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.create()

        mk_conn.cursor.assert_called_once_with()
        SQL = """INSERT INTO tb_invoice ( {0} ) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s )""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL, ANY)

        mk_conn.commit.assert_called_once_with()
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_create_with_error_AlreadyExists(self):

        mk_conn = Mock()
        obj = Invoce(self.data, Mock())
        obj.Id = 10

        self.assertRaises(AlreadyExists, obj.create)

    def test_Invoce_call_filter_all(self):

        mk_conn = Mock()
        mk_cursor = MagicMock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.filter()

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Id, {0} FROM tb_invoice WHERE IsActive=1 LIMIT 20 OFFSET 0""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL)
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_filter_by_query_case_1(self):

        mk_conn = Mock()
        mk_cursor = MagicMock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.filter(query={"Document": "DOC001"}, size=30, offset=10)

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Id, {0} FROM tb_invoice WHERE IsActive=1 AND Document = 'DOC001' LIMIT 30 OFFSET 10""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_filter_by_query_case_2(self):

        mk_conn = Mock()
        mk_cursor = MagicMock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.filter(
            query={"Document": "DOC001", "ReferenceYear": 2019}, size=30, offset=10
        )

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Id, {0} FROM tb_invoice WHERE IsActive=1 AND Document = 'DOC001' AND ReferenceYear = '2019' LIMIT 30 OFFSET 10""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_filter_by_query_and_order_case_1(self):

        mk_conn = Mock()
        mk_cursor = MagicMock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.filter(
            query={"Document": "DOC001", "ReferenceYear": 2019},
            order=["Document"],
            size=30,
            offset=10,
        )

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Id, {0} FROM tb_invoice WHERE IsActive=1 AND Document = 'DOC001' AND ReferenceYear = '2019' ORDER BY Document LIMIT 30 OFFSET 10""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL)
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_filter_by_query_and_order_case_2(self):

        mk_conn = Mock()
        mk_cursor = MagicMock()
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce(self.data, mk_conn)
        obj.filter(
            query={"Document": "DOC001", "ReferenceYear": 2019},
            order=["Document", "ReferenceYear"],
            size=30,
            offset=10,
        )

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Id, {0} FROM tb_invoice WHERE IsActive=1 AND Document = 'DOC001' AND ReferenceYear = '2019' ORDER BY Document, ReferenceYear LIMIT 30 OFFSET 10""".format(
            ", ".join(self.data.keys())
        )
        mk_cursor.execute.assert_called_once_with(SQL)
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_update(self):
        mk_conn = Mock()
        mk_cursor = Mock()
        mk_conn.cursor.return_value = mk_cursor

        n_data = self.data.copy()
        n_data["Id"] = 10
        obj = Invoce(n_data, mk_conn)
        obj.update()

        mk_conn.cursor.assert_called_once_with()
        SQL = """UPDATE tb_invoice SET {0} WHERE id = 10""".format(
            ", ".join(["%s='%s'" % (k, v) for k, v in self.data.items()])
        )
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_conn.commit.assert_called_once_with()
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_update_with_error_DoesNotExist(self):

        obj = Invoce(self.data, Mock())
        self.assertRaises(DoesNotExist, obj.update)

    def test_Invoce_call_delete(self):
        mk_conn = Mock()
        mk_cursor = Mock()
        mk_conn.cursor.return_value = mk_cursor

        n_data = self.data.copy()
        n_data["Id"] = 10
        n_data["DeactiveAt"] = datetime.now()
        obj = Invoce(n_data, mk_conn)
        obj.delete()

        mk_conn.cursor.assert_called_once_with()
        SQL = """UPDATE tb_invoice SET IsActive=False, DeactiveAt='{0}' WHERE id = 10""".format(
            n_data["DeactiveAt"]
        )
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_conn.commit.assert_called_once_with()
        mk_cursor.close.assert_called_once_with()

    def test_Invoce_call_delete_with_error_DoesNotExist(self):

        obj = Invoce(self.data, Mock())
        self.assertRaises(DoesNotExist, obj.delete)

    def test_Invoce_call_count(self):
        mk_conn = Mock()
        mk_cursor = Mock()
        mk_cursor.fetchone.return_value = {"count": 10}
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce({}, mk_conn)
        result = obj.count()

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Count(*) as count FROM tb_invoice WHERE IsActive=1"""
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_cursor.close.assert_called_once_with()
        self.assertEqual(result, 10)

    def test_Invoce_call_count_by_query(self):
        mk_conn = Mock()
        mk_cursor = Mock()
        mk_cursor.fetchone.return_value = {"count": 10}
        mk_conn.cursor.return_value = mk_cursor

        obj = Invoce({}, mk_conn)
        result = obj.count({"Document": "DOC001"})

        mk_conn.cursor.assert_called_once_with()
        SQL = """SELECT Count(*) as count FROM tb_invoice WHERE IsActive=1 AND Document = 'DOC001'"""
        mk_cursor.execute.assert_called_once_with(SQL)

        mk_cursor.close.assert_called_once_with()
        self.assertEqual(result, 10)
