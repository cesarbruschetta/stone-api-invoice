""" """
import unittest
from unittest.mock import Mock
from datetime import datetime

from invoiceapi.utils import default_datetime


class TestUtils(unittest.TestCase):
    def test_utils_call_default_datetime_with_callable(self):

        mk_call = Mock()
        default_datetime(mk_call)
        mk_call.assert_called_once_with()

    def test_utils_call_default_datetime_with_str_to_datetime(self):

        result = default_datetime("2019-09-15T15:53:00")

        self.assertIsInstance(result, datetime)

        self.assertEqual(result.year, 2019)
        self.assertEqual(result.month, 9)
        self.assertEqual(result.hour, 15)

    def test_utils_call_default_datetime_with_str_invalid(self):

        result = default_datetime("2019-09-15")
        self.assertIsNone(result)

    def test_utils_call_default_datetime_with_value_None(self):

        result = default_datetime(None)
        self.assertIsNone(result)
