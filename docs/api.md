## Invoice API

### About
| Url                                                    | Version |
| ------------------------------------------------------ | ------- |
| [http://0.0.0.0:8080/](http://0.0.0.0:8080/ "API url") | 0.1     |

### Schemes
| Scheme |
| ------ |

## Endpoints
## /
## GET
###


### Expected Response Types
| Response | Reason                |
| -------- | --------------------- |
|          |                       |
| default  | application/json |

### Parameters
| Name | In | Description | Required? | Type |
| ---- | -- | ----------- | --------- | ---- |

### Content Types Produced
| Produces         |
| ---------------- |
| application/json |


## invoices
## GET
###


### Expected Response Types
| Response | Reason                |
| -------- | --------------------- |
|          |                       |
| default  | application/json |


### Content Types Produced
| Produces         |
| ---------------- |
| application/json |

## POST
###


### Expected Response Types
| Response | Reason                                              |
| -------- | --------------------------------------------------- |
| 200      | Successfully created Invoice                        |
| 400      | Error processing payload, Invoice cannot be created |

### Parameters
| Name                  | In   | Description | Required? | Type   |
| --------------------- | ---- | ----------- | --------- | ------ |
| RegisterInvoiceSchema | body |             | true      | object |

### Content Types Produced
| Produces         |
| ---------------- |
| application/json |


## invoices/{invoice_id}
## DELETE
###
Delete Invoice by Id, DeactiveAt is an optional parameter

### Expected Response Types
| Response | Reason                                              |
| -------- | --------------------------------------------------- |
|          |                                                     |
| 204      | Successfully deleted invoice                        |
| 404      | Invoice not found                                   |
| 400      | Error processing payload, Invoice cannot be deleted |

### Parameters
| Name                | In   | Description | Required? | Type   |
| ------------------- | ---- | ----------- | --------- | ------ |
| DeleteInvoiceSchema | body |             | true      | object |

### Content Types Produced
| Produces         |
| ---------------- |
| application/json |
