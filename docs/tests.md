# Teste da aplicação

## Instalar dependencias para os testes

```bash
$ cd /path/to/env/stone-api-invoice
$ ../bin/pip install -e .
$ ../bin/pip install coverage WebTest
```

## Executar unittest

```bash
$ cd /path/to/env/stone-api-invoice
$ ../bin/python setup.py test -v
```

## Executar coverage

```bash
$ cd /path/to/env/stone-api-invoice
$ ../bin/coverage run -m unittest -v
```

## Executar relatorio

```bash
$ cd /path/to/env/stone-api-invoice
$  coverage report
```

## Analise pelo Coverage

|Name                                      |Stmts  | Miss |Branch |BrPart | Cover|
|------------------------------------------|-------|------|-------|-------|------|
|invoiceapi/__init__.py                    |   0   |  0   |   0   |   0   | 100% |
|invoiceapi/exceptions.py                  |   4   |  0   |   0   |   0   | 100% |
|invoiceapi/models.py                      |  83   |  0   |  32   |   1   |  99% |
|invoiceapi/restfulapi.py                  |  97   |  7   |  12   |   1   |  93% |
|invoiceapi/utils.py                       |  26   |  3   |  10   |   2   |  86% |
|TOTAL                                     |  210  |  10  |  54   |   4   |  95% |
