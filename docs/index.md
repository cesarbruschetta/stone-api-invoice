# Documentação

## Descrição

O desafio que propomos é escrever uma API RESTful utilizando python 3.7, de um resource para Invoice (Nota Fiscal).

## Menu

* [Setup](./setup.md)
* [Configuração](./configuration.md)
* [Testes](./tests.md)
* [API](./api.md)

## Criador

* Cesar Augusto
* cesarabruschetta@gmail.com
* Developer Python
