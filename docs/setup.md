# Setup

## Instalação em Ambiente Local

### Criar o virtualenv

```bash
$ virtualenv -p /usr/bin/python3.7 stone-api-invoice
```

### Clonar o código fonte da aplicação

```bash
$ cd stone-api-invoice
$ git clone https://gitlab.com/cesarbruschetta/stone-api-invoice.git app
```

### Instalar as dependências do python

```bash
$ cd app
$ ../bin/pip install -r requirements.txt
$ ../bin/pip install -e .
```

### Iniciar o processo para desenvolvimento
```bash
$ cd stone-api-invoice/app
$ ../bin/pserver development.ini
```

## Instalação em Ambiente Docker

### Clonar o código fonte da aplicação

```bash
$ git clone https://gitlab.com/cesarbruschetta/stone-api-invoice.git stone-api-invoice
```

### Realizando o `build` da imagem docker

```bash
$ cd stone-api-invoice
$ docker-compose build . -t api-invoice:latest
```

### Rotando a imagem docker atraves do `docker-compose`

```bash
$ cd challenge-graph-labs
$ docker-compose up -d
```
*PS* Esse `docker-compose` iniciara um conteiner para a aplicação e um conteiner para o MySQL (Banco de dados)


## Configurando o MySQL (Banco de dados)

Para que a aplicação funcione perfeitamente, é necessario criar o `banco de dados` e a `tabela` de para os Invoice, para isso utilize o console de gerenciamento do MySQL como exemplicado abaixo

#### Comando Acessar o console do gerenciamento

```bash
$ mysql -u <USER> -p -h <HOST> -b <DATA_BASE>
```

#### Comando para criar o `banco de dados`

```sql
CREATE DATABASE `invoice-api`;
```

#### Comando para criar a `tabela`

```sql
USE `invoice-api` ;
CREATE TABLE `tb_invoice` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`CreatedAt` DATETIME NOT NULL,
	`ReferenceMonth` INT NOT NULL,
	`ReferenceYear` INT NOT NULL,
	`Document` VARCHAR(14) NOT NULL,
	`Description` VARCHAR(256) NOT NULL,
	`Amount` DECIMAL(16,2) NOT NULL,
	`IsActive` TINYINT NOT NULL DEFAULT '1',
	`DeactiveAt` DATETIME,
	PRIMARY KEY (`Id`)
);
```

