# Configuração da aplicação

## Overview

A configuração é feita através do arquivo `.ini` na raiz do projeto. Para ambiente de *desenvolvimento* deve se utilizar como base o arquivo
`development.ini` e para ambiente de *produção* deve se utilizar como base o arquivo `production.ini`

Nesse arquivos a parte que deve ser alterada é referente à conexão com o banco de dados, onde deve se mudar as diretivas mostradas abaixo, que estao na sessão `[app:main]` do arquivo e tambem deve se adicionar o `TOKEN` para acesso na api.

```
[app:main]

...

api.mysqldb.user=<USUARIO DO DB>
api.mysqldb.password=<SENHA DO DB>
api.mysqldb.host=<ENDEREÇO SERVIDOS DE DB>
api.mysqldb.database=<NOME DO DB>
api.mysqldb.port=<POSTA DO SERVIDO DE DB>

api.auth.token=<TOKEN DA API>
...
```

