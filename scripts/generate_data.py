import http.client
import json

headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-API-Token": "ca3d8e11-4804-42ed-a29a-42dfaa075880",
}

conn = http.client.HTTPConnection("0.0.0.0", 8080)


for i in range(100):
    payload = json.dumps(
        {
            "ReferenceMonth": 10 if i % 2 else 9,
            "ReferenceYear": "2019",
            "Document": "DOC00%s" % i,
            "Description": "DOC Invoce Test %s" % i,
            "Amount": 100,
        }
    )
    conn.request("POST", "/invoices", payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(data.decode("utf-8"))
