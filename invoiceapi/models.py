""" module to models class """
from datetime import datetime
from decimal import Decimal
from typing import Dict, List

from pyramid.settings import asbool

from invoiceapi.utils import default_datetime
from invoiceapi.exceptions import AlreadyExists, DoesNotExist, NonDatabaseConnection


class Invoce:
    """
    handling class for data stored in mysql database


    Schema:
        CreatedAt: DATETIME,
        ReferenceMonth: INTEGER,
        ReferenceYear: INTEGER,
        Document: VARCHAR(14),
        Description: VARCHAR(256),
        Amount: DECIMAL(16, 2),
        IsActive: TINYINT,
        DeactiveAt: DATETIME

    """

    _table = "tb_invoice"
    _fields = [
        ("Id", int, 0),
        ("CreatedAt", default_datetime, datetime.now),
        ("ReferenceMonth", int, 0),
        ("ReferenceYear", int, 0),
        ("Document", str, ""),
        ("Description", str, ""),
        ("Amount", Decimal, Decimal(0.0)),
        ("IsActive", asbool, True),
        ("DeactiveAt", default_datetime, None),
    ]
    _connector = None

    def __init__(self, data: Dict, connector) -> None:
        """
        A `dict` impute is expected with the data in the schema below and
        a database connection object
        """
        self.parse_fields(fields=self._fields, data=data)
        self._connector = connector

    def parse_fields(self, fields: List, data: Dict) -> None:
        """
        Scans and returns obj model to object.
        The `fields` argument should receive an associative list in the form:

          [
            (<collum>, <conversion function>, <default value>),
          ]
        """
        cfg = list(fields)

        for collum, convert, default in cfg:
            value = data.get(collum, default)
            if convert is not None:
                value = convert(value)

            setattr(self, collum, value)

    @property
    def connector(self):
        if not self._connector:
            raise NonDatabaseConnection("Database connection not defined")
        return self._connector

    @property
    def data(self):
        return {f[0]: getattr(self, f[0]) for f in self._fields if f[0] != "Id"}

    def create(self):

        if self.Id:
            raise AlreadyExists(
                "Invoice %s already exists in the database, not create" % (self.Id)
            )

        data = self.data
        SQL = """INSERT INTO {0} ( {1} ) VALUES ( {2} )""".format(
            self._table,
            ", ".join(data.keys()),
            ", ".join(["%s" for i in range(len(data.keys()))]),
        )
        cnx = self.connector
        cursor = cnx.cursor()
        cursor.execute(SQL, tuple(data.values()))
        # Make sure data is committed to the database
        cnx.commit()
        self.Id = cursor.lastrowid
        cursor.close()
        return self.Id

    def update(self):
        if not self.Id:
            raise DoesNotExist(
                "Invoice %s does not exist in database, not update" % (self.Id)
            )

        data = self.data
        SQL = """UPDATE {0} SET {1} WHERE id = {2}""".format(
            self._table,
            ", ".join(["%s='%s'" % (k, v) for k, v in data.items()]),
            self.Id,
        )

        cnx = self.connector
        cursor = cnx.cursor()
        cursor.execute(SQL)
        # Make sure data is committed to the database
        cnx.commit()
        cursor.close()
        return cursor.lastrowid

    def filter(self, query={}, order=(), size=20, offset=0):

        where = ""
        if query:
            where = " AND {0}".format(
                " AND ".join(["%s = '%s'" % (k, v) for k, v in query.items()])
            )

        order_by = ""
        if order:
            order_by = "ORDER BY {0} ".format(", ".join(order))

        SQL = """SELECT {0} FROM {1} WHERE IsActive=1{2} {3}LIMIT {4} OFFSET {5}""".format(
            ", ".join([f[0] for f in self._fields]),
            self._table,
            where,
            order_by,
            size,
            offset,
        )

        cnx = self.connector
        cursor = cnx.cursor()
        cursor.execute(SQL)

        result = [dict(row) for row in cursor.fetchall()]
        cursor.close()
        return result

    def count(self, query={}):
        where = ""
        if query:
            where = " AND {0}".format(
                " AND ".join(["%s = '%s'" % (k, v) for k, v in query.items()])
            )

        SQL = """SELECT Count(*) as count FROM {0} WHERE IsActive=1{1}""".format(
            self._table, where
        )

        cnx = self.connector
        cursor = cnx.cursor()
        cursor.execute(SQL)

        result = cursor.fetchone()
        cursor.close()
        return result["count"]

    def delete(self):

        if not self.Id:
            raise DoesNotExist(
                "Invoice %s does not exist in database, not delete" % (self.Id)
            )

        SQL = """UPDATE {0} SET IsActive=False, DeactiveAt='{1}' WHERE id = {2}""".format(
            self._table, self.DeactiveAt or datetime.now(), self.Id
        )
        cnx = self.connector
        cursor = cnx.cursor()

        cursor.execute(SQL)
        # Make sure data is committed to the database
        cnx.commit()
        cursor.close()
