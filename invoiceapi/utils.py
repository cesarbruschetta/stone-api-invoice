""" """
import os
import binascii
from datetime import datetime

from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.threadlocal import get_current_registry


def valid_token(request, **kargs):

    header = "X-API-Token"
    htoken = request.headers.get(header)
    if htoken is None:
        raise HTTPUnauthorized()

    settings = get_current_registry().settings
    VALID_API_KEY = [settings.get("api.auth.token")]

    valid = htoken in VALID_API_KEY
    if not valid:
        raise HTTPUnauthorized()


def default_datetime(value):

    if callable(value):
        return value()
    elif isinstance(value, str):
        try:
            return datetime.strptime(value, "%Y-%m-%dT%H:%M:%S")
        except ValueError:
            return None

    return value


def expandvars_dict(settings):
    """Expands all environment variables in a settings dictionary."""
    return dict(
        (key, os.path.expandvars(str(value))) for key, value in settings.items()
    )
