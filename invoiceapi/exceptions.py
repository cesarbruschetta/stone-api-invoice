""" module to exceptions class """


class NonRetryableError(Exception):
    """Error which cannot be recovered without modifying data state
    on the client side, e.g., requested resource does not exist, invalid URI, etc.
    """


class AlreadyExists(NonRetryableError):
    """Error representing attempted registration of an entity whose
    identifier is already in use.
    """


class DoesNotExist(NonRetryableError):
    """Error representing attempting to retrieve an entity from
    of an identifier that is not associated with any of them.
    """


class NonDatabaseConnection(NonRetryableError):
    """Error from which database connection has not been defined and reported
    the data handling class
    """
