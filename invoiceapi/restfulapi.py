""" module to webserve api in restful """
import datetime
import decimal
from urllib.parse import urlencode

import MySQLdb
import MySQLdb.cursors
import colander
from pyramid.renderers import JSON
from pyramid.config import Configurator
from pyramid.httpexceptions import HTTPNoContent, HTTPBadRequest, HTTPNotFound
from cornice import Service
from cornice.validators import colander_body_validator
from cornice.service import get_services
from cornice_swagger import CorniceSwagger

from invoiceapi.models import Invoce
from invoiceapi.utils import valid_token, expandvars_dict
from invoiceapi.exceptions import DoesNotExist


swagger = Service(
    name="Invoice API", path="/", description="API RESTful para Invoice (Nota Fiscal)"
)

invoices = Service(name="invoices", path="/invoices", description="teste")

invoice = Service(
    name="invoice",
    path="/invoices/{invoice_id}",
    description="Get the invoice data and manipulaite.",
)


class RegisterInvoiceSchema(colander.MappingSchema):
    """Represent the data schema for invoice registration.
    """

    CreatedAt = colander.SchemaNode(colander.DateTime(), missing=colander.drop)
    ReferenceMonth = colander.SchemaNode(colander.Int())
    ReferenceYear = colander.SchemaNode(colander.Int())
    Document = colander.SchemaNode(colander.String())
    Description = colander.SchemaNode(colander.String())
    Amount = colander.SchemaNode(colander.Float())


class DeleteInvoiceSchema(colander.MappingSchema):
    """Represent the data schema for invoice delete.
    """

    DeactiveAt = colander.SchemaNode(colander.DateTime(), missing=colander.drop)


@invoices.get(accept="application/json", renderer="json", validators=(valid_token))
def list_invoices_data(request):
    """ List Invoice and filter or order by ReferenceMonth, ReferenceYear, Document """
    limit = 30

    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        raise HTTPBadRequest("page must be integer")

    query = {}
    for parameter in ["ReferenceMonth", "ReferenceYear", "Document"]:
        value = request.GET.get(parameter)
        if value:
            query[parameter] = value

    order = request.GET.get("order_by") and request.GET.get("order_by").split(",") or []

    obj = Invoce({}, request.connection)
    previous_url = ""
    next_url = ""

    total = obj.count(query=query)
    query_string = {}
    query_string["order_by"] = ", ".join(order)
    query_string.update(query)

    if total - limit and total:
        query_string.update({"page": page + 1})
        next_url = "/invoices?{0}".format(urlencode(query_string))

    if page > 1 and total:
        query_string.update({"page": page - 1})
        previous_url = "/invoices?{0}".format(urlencode(query_string))

    offset = (page - 1) * limit
    results = obj.filter(query=query, size=limit, order=order, offset=offset)

    return {
        "total": total,
        "size": len(results),
        "results": results,
        "next": next_url,
        "previous": previous_url,
    }


@invoices.post(
    schema=RegisterInvoiceSchema(),
    validators=(colander_body_validator, valid_token),
    response_schemas={
        "200": RegisterInvoiceSchema(description="Successfully created Invoice"),
        "400": RegisterInvoiceSchema(
            description="Error processing payload, Invoice cannot be created"
        ),
    },
    accept="application/json",
    renderer="json",
)
def create_invoice(request):
    """ Create Invoice by Payload Validate """
    try:
        obj = Invoce(request.validated, request.connection)
        obj.create()

        request.response.status = 201
        return {"msg": "invoice created successfuly", "id": obj.Id}

    except Exception as ex:
        raise HTTPBadRequest("cannot create invoice : %s" % (str(ex)))


@invoice.delete(
    schema=DeleteInvoiceSchema(),
    validators=(colander_body_validator, valid_token),
    accept="application/json",
    renderer="json",
    response_schemas={
        "204": DeleteInvoiceSchema(description="Successfully deleted invoice"),
        "404": DeleteInvoiceSchema(description="Invoice not found"),
        "400": DeleteInvoiceSchema(
            description="Error processing payload, Invoice cannot be deleted"
        ),
    },
)
def delete_invoice(request):
    """ Delete Invoice by Id, DeactiveAt is an optional parameter """
    try:
        data = request.validated
        data["Id"] = request.matchdict["invoice_id"]
        obj = Invoce(data, request.connection)
        obj.delete()
    except DoesNotExist as exc:
        raise HTTPNotFound(exc)
    except Exception as ex:
        raise HTTPBadRequest("cannot delete invoice : %s" % (str(ex)))

    request.response.status = 204
    raise HTTPNoContent("invoice deleted successfully")


@swagger.get()
def openAPI_spec(request):
    doc = CorniceSwagger(get_services())
    doc.summary_docstrings = True
    return doc.generate("Invoice API", "0.1")


def main(global_config, **settings):

    settings = expandvars_dict(settings)
    config = Configurator(settings=settings)
    config.include("cornice")
    config.include("cornice_swagger")

    json_renderer = JSON()

    def datetime_adapter(obj, request):
        return obj.isoformat()

    def decimal_adapter(obj, request):
        return str(obj)

    json_renderer.add_adapter(datetime.datetime, datetime_adapter)
    json_renderer.add_adapter(decimal.Decimal, decimal_adapter)
    config.add_renderer("json", json_renderer)
    config.scan()

    connection = MySQLdb.connect(
        user=settings["api.mysqldb.user"],
        passwd=settings["api.mysqldb.password"],
        host=settings["api.mysqldb.host"],
        db=settings["api.mysqldb.database"],
        port=int(settings["api.mysqldb.port"]),
        cursorclass=MySQLdb.cursors.DictCursor,
    )

    config.add_request_method(lambda request: connection, "connection", reify=True)

    return config.make_wsgi_app()
