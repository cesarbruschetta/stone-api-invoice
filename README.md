# Desafio

O desafio que propomos é escrever uma API RESTful utilizando python 3.7, de um resource para Invoice (Nota Fiscal). Lembrando que você deve responder este e-mail com um prazo para recebermos o desafio solucionado.

Abaixo, algumas especificações:

- Não é permitido o uso de ORM;
- Atenção para utilizar os status codes do HTTP corretamente de acordo com cada operação da API;
- Temos que autenticar através de token de aplicação;

- Na listagem de invoices:
     - Podemos fazer o filtro por mês, ano ou documento;
     - Tem que ter paginação;
     - Podemos escolher a ordenação por mês, ano, documento ou combinações entre eles;

- A API deve possuir testes de unidade que não consumam o recurso real, ou seja, deve ser feito mock;
- O método DELETE não executa uma deleção física, e sim uma deleção lógica, ou seja, podemos apenas desativar uma invoice;
- Utilizar MySQL como Banco de dados;

Domínio da tabela Invoice:
{
    CreatedAt: DATETIME,
    ReferenceMonth: INTEGER,
    ReferenceYear: INTEGER,
    Document: VARCHAR(14),
    Description: VARCHAR(256),
    Amount: DECIMAL(16, 2),
    IsActive: TINYINT,
    DeactiveAt: DATETIME
}